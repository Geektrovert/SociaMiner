import argparse
import json
import nltk
import string
import tokenizer

from collections import Counter
from nltk import bigrams
from nltk.corpus import stopwords

def get_parser():
	parser = argparse.ArgumentParser(description="Console data printer")
	parser.add_argument("-q",
		"--query",
		dest = "query",
		help = "Filename to read"
		)
	return parser

if __name__ == '__main__':
	parser = get_parser()
	args = parser.parse_args()
	punctuation = list(string.punctuation)
	stop = stopwords.words('english') + punctuation + ['rt','RT','via','.','’','_','-','@','…']
	try:
		with open('data\\' + args.query, 'r') as f:
			count_all = Counter()
			count_hashes = Counter()
			count_mentions = Counter()
			count_bigrams = Counter()
			for line in f:
				tweet = json.loads(line)
				terms_hash = [term for term in tokenizer.preprocess(tweet['text']) if term not in stop and term.startswith('#')]
				terms_mentions = [term for term in tokenizer.preprocess(tweet['text']) if term not in stop and term.startswith('@')]
				terms_all = [term for term in tokenizer.preprocess(tweet['text']) if term not in stop and not term.startswith(('#','@'))]
				bigram_terms = bigrams(terms_all)
				count_hashes.update(terms_hash)
				count_mentions.update(terms_mentions)
				count_all.update(terms_all)
				count_bigrams.update(bigram_terms)
			print("Words:")
			print(count_all.most_common(10))
			print("")
			print("Hashtags:")
			print(count_hashes.most_common(10))
			print("")
			print("Mentions:")
			print(count_mentions.most_common(10))
			print("")
			print("Bigrams:")
			print(count_bigrams.most_common(5))
			print("")
	except Exception as e:
		print("Error: %s" % (str(e)))
