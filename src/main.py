import argparse
import config
import json
import string
import tweepy
import time

from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener

count = 0

def get_parser():
	parser = argparse.ArgumentParser(description="Twitter Downloader")
	parser.add_argument("-q",
		"--query",
		dest="query",
		help="Query/Filter",
		default="-")
	parser.add_argument("-d",
		dest="data_dir",
		help="Output/Data Directory")
	return parser

def authenticate():
	authenticator = OAuthHandler(config.ConsumerKey, config.ConsumerSecret)
	authenticator.set_access_token(config.AccessToken, config.AccessSecret)
	api = tweepy.API(authenticator,wait_on_rate_limit = True)
	return authenticator

class TListener(StreamListener):
	count = 0
	def __init__(self,data_dir,query):
		query_fname = format_filename(query)
		self.outfile = "%s/stream_%s.json" % (data_dir,query_fname)

	def on_data(self,data):
		try:
			with open(self.outfile,'a') as f:
				f.write(data.rstrip()+'\n')
				self.count = self.count + 1
				print(self.count)
				return True
		except Exception as e:
			print("Error on_data: ",str(e))
			time.sleep(5)
			return True
	def on_error(self,status_code):
		if status_code is 420:
			return False

def format_filename(fname):
	return ''.join(convert_valid(ch) for ch in fname)

def convert_valid(ch):
	valid = "-_.%s%s" % (string.ascii_letters,string.digits)
	if ch in valid:
		return ch
	else:
		return '_'

@classmethod
def parse(cls,api,raw):
	status = cls.first_parse(api,raw)
	setattr(status,'json',json.dumps(raw))
	return status

def main():
	parser = get_parser()
	args = parser.parse_args()
	authenticator = authenticate()
	twitter_stream = Stream(authenticator,TListener(args.data_dir,args.query))
	twitter_stream.filter(track=[args.query], async = True)

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt as e:
		print("Program ended!")
